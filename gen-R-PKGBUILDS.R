get_pkg_var <- function(pkg, var){
  suppressWarnings(
    ## Non-zero exit code if the variable doesn't exist, but we don't care
    system2("git",
            paste0("config --file .gitmodules --get submodule.PKGBUILDS/r-",
                   pkg, ".", var),
            stdout = TRUE, stderr = NULL))
}


mk_deps_suggests <- function(x, name, optdeps = FALSE) {
  x <- unlist(strsplit(x, ",[[:space:]]*"))
  x <- gsub("^R$", NA, x, fixed = TRUE)
  x <- gsub("R[[:space:]]+\\(.*", NA, x)
  x <- gsub("[[:space:]]*NA", NA, x)
  x <- gsub("\\n", "", x)
  x <- x[!is.na(x)]
  if (optdeps) {
    ## optdepends does not support specifying minimum versions
    x <- sub("[ (].*", "", x)
  } else{
    ## Add minimum version
    x <- gsub("(.*?)[[:space:]]?\\(>=[[:space:]]?(.*?)\\)", "\\1>=\\2", x)
    ## substitute colons or hyphons with a period. TODO: Are these allowed in package names?
    x <- gsub("[:-]", ".", x)
  }
  ## Base packages:
  base_name <- unlist(lapply(strsplit(x, ">="), `[[`, 1))
  x <- x[!base_name %in% c("base", "boot", "class", "cluster", "codetools",
                           "compiler", "datasets", "foreign", "graphics",
                           "MASS", "Matrix", "methods", "mgcv", "nlme",
                           "grDevices", "grid", "KernSmooth", "lattice",
                           "nnet", "parallel", "rpart", "spatial", "splines",
                           "stats", "stats4", "survival", "tcltk", "tools",
                           "translations", "utils")]
  x <- tolower(x)
  base_name <- unlist(lapply(strsplit(x, ">="), `[[`, 1))
  ## Sometimes packages have depends twice (e.g. in Depends and
  ## LinkingTo), remove one:
  x <- x[!duplicated(base_name)]
  base_name <- base_name[!duplicated(base_name)]
  ## packages named r-cran-*:
  x <- ifelse(tolower(base_name) %in% c("dicekriging", "extrafont",
                                        "extrafontdb", "rinside",
                                        "rttf2pt1"),
              paste0("'r-cran-", x, "'"),
              paste0("'r-", x, "'"))
  rpkgs <- paste0(x, collapse = " ")
  ## Remove if we didn't find anything
  if (rpkgs == "'r-'") rpkgs <- NULL
  var <- if (optdeps) {
    "optdepends"
  } else "depends"
  other_pkgs <- get_pkg_var(name, var)
  ## if (nchar(other_pkgs) > 0)
  x <- paste0(other_pkgs, " ", rpkgs)
  x <- trimws(x)
  ## Only write depends if we actually have something
  if (optdeps & nchar(x) > 0){
    x <- paste0("optdepends=(", x, ")")
  } else if (!optdeps){
    x <- paste0("depends=('r' ", x, ")")
  }
  x
}

get_makedepends <- function(x){
  ## x should be a character string like "tidyverse"
  makedepends <- get_pkg_var(x, "makedepends")
  if (length(makedepends) > 0){
    paste0("makedepends=(", makedepends, ")")
  } else ""
}

sub_license <- function(x){
  ## Make x a character vector where each element is a license:
  x <- unlist(strsplit(x, " \\| "))
  ## (L)GPL get treated specially:
  if (sum(x == c("GPL-2", "GPL-3")) == 2){
    return("license=('GPL')")
  } else if (sum(x == c("LGPL-2", "LGPL-3")) == 2){
    return("license=('LGPL')")
  }

  x <- ifelse(x %in% c("GPL (>= 3)",
                      "GPL-3",
                      "GNU General Public License version 3",
                      "GNU General Public License",
                      "GPL (>= 2.15.1)",
                      "GPL (>= 3.0)"),
             "GPL3",
             ifelse(x %in% c("GPL (>= 2)",
                             "GPL (>= 2.0)"),
                    "GPL",
                    ifelse(x %in% c("GPL-2"),
                           "GPL2",
                           ifelse(x %in% c("Free BSD",
                                           "FreeBSD",
                                           "BSD_3_clause + file LICENSE"),
                                  "BSD",
                                  ifelse(x %in% c("LGPL (>= 3)",
                                                  "LGPL-3",
                                                  "LGPL (>= 3.0)"),
                                         "LGPL3",
                                         ifelse(x %in% c("LGPL (>= 2)",
                                                         "LGPL (>= 2.0)"),
                                                "LGPL",
                                                ifelse(x %in% c("Apache License 2.0",
                                                                "Apache License",
                                                                "Apache License (== 2.0)"),
                                                       "Apache",
                                                       ifelse(x %in% c("MIT", "MIT + file LICENSE"),
                                                              "MIT",
                                                              ifelse(x %in% c("Artistic-2.0"),
                                                                     "Artistic2.0",
                                                                     ifelse(x %in% c("Mozilla Public License"),
                                                                            "MPL2",
                                                                            ifelse(x %in% c("file LICENSE"),
                                                                                   "custom", x)))))))))))
  paste0("license=(",
         paste0("'", x, "'", collapse = " "),
         ")")
}

clean_pkgdesc <- function(desc, name){
  ## Stupidly remove all quotes and cut the desc at 80 chars
  desc <- gsub("'", "", desc)
  desc <- gsub('"', "", desc)
  desc <- gsub('\n', " ", desc)
  if (nchar(desc) > 80) x <- substr(desc, 1, 80)
  desc
}

determine_arch <- function(pkg){
  if (tolower(pkg) == "yes"){
    return("arch=('x86_64')")
  } else return("arch=('any')")
}

gen_replaces <- function(pkg){
  ifelse(pkg %in% c(
    "bit",
    "bitops",
    "catools",
    "coda",
    "depmix",
    "distr",
    "expm",
    "gnumeric",
    "linkcomm",
    "msm",
    "mvtnorm",
    "random",
    "rgl",
    "rlang",
    "scatterplot3d",
    "sfsmisc",
    "startupmsg",
    "sweavelistingutils",
    "tnet",
    "wikibooks",
    "xtable",
    "xml"
  ), paste0("\nreplaces=('", "r-cran-", pkg, "')"),
  "")
}

make_pkgbuild <- function(pkg) {
  cran_name <- pkg[["Package"]]
  pkg_name <- tolower(pkg[["Package"]])
  cran_ver <- pkg[["Version"]]
  pkg_tar <- paste0(cran_name, "_\"$_cranver\".tar.gz")
  variables <- paste(
    "# Maintainer: Alex Branham <alex.branham@gmail.com>",
    paste0("_cranver=", cran_ver),
    ## pkgname
    paste0("pkgname=r-", pkg_name),
    ## pkgver
    "pkgver=${_cranver//[:-]/.}",
    ## pkgrel
    "pkgrel=1",
    ## pkgdesc
    paste0("pkgdesc='", clean_pkgdesc(pkg[["Title"]], pkg_name), "'"),
    ## arch
    determine_arch(pkg[["NeedsCompilation"]]),
    ## url
    paste0("url='https://cran.r-project.org/package=", cran_name, "'"),
    ## license
    sub_license(pkg[["License"]]),
    ## depends
    mk_deps_suggests(paste0(pkg[["Depends"]], ", ", pkg[["Imports"]], ", ", pkg[["LinkingTo"]]), pkg_name),
    ## optdepends
    mk_deps_suggests(pkg[["Suggests"]], pkg_name, TRUE),
    ## makedepends
    get_makedepends(pkg_name),
    ## replaces
    gen_replaces(pkg_name),
    ## source
    paste0("source=(\"https://cran.r-project.org/src/contrib/", cran_name, "_\"$_cranver\".tar.gz\")"),
    ## md5sums
    paste0("md5sums=(", paste0("'", pkg[[65]], "'"), ")"),
    sep = "\n")
  ## Remove empty lines
  variables <- gsub("\n{2,}", "\n", variables)

  functions <- paste(
    "",
    "build(){",
    paste0("    R CMD INSTALL ", cran_name, "_\"$_cranver\".tar.gz -l \"$srcdir\""),
    "}",
    "package() {",
    "    install -dm0755 \"$pkgdir\"/usr/lib/R/library",
    paste0("    cp -a --no-preserve=ownership ", cran_name, " \"$pkgdir\"/usr/lib/R/library"),
    "}",
    "",
    sep = "\n")
  paste(variables, functions, sep = "\n")
}

write_pkgbuild <- function(pkg){
  name <- pkg["Package"]
  dir <- paste0("PKGBUILDS/r-", tolower(name))
  dir.create(dir, showWarnings = FALSE)
  PKGBUILD <- make_pkgbuild(pkg)
  writeLines(PKGBUILD, paste0(dir, "/PKGBUILD"))
}

whitelist <- function(){
  whitelist <- system2("git",
                      c("config --file .gitmodules  --name-only --get-regexp path"),
                      stdout = TRUE, stderr = NULL)
  ## Remove submodule.PKGBUILDS/r-<pkgname>.path
  whitelist <- substr(whitelist, nchar("submodule.PKGBUILDS/r-") + 1, nchar(whitelist) - 5)
  whitelist
}

write_all_pkgbuilds <- function(){
  av <- tools::CRAN_package_db()
  av <- av[tolower(av$Package) %in% whitelist(), ]
  apply(av, 1, write_pkgbuild)
  system("git submodule foreach 'makepkg --printsrcinfo > .SRCINFO'")
  message("Done!")
}

current_pkgvers <- function(){
  whitelist <- whitelist()
  files <- paste0("PKGBUILDS/r-", whitelist, "/PKGBUILD")
  current_vers <- lapply(files, function(x){
    ## get line that starts with _cranver
    cranver <- readLines(x)
    cranver <- cranver[which(startsWith(cranver, "_cranver"))]
    current_ver <- gsub("_cranver=", "", cranver)
  })
  current_vers <- as.character(current_vers)
  names(current_vers) <- whitelist
  current_vers <- as.data.frame(current_vers)
  current_vers$Package <- rownames(current_vers)
  rownames(current_vers) <- NULL
  current_vers
}

update_pkgs <- function(){
  ## Get newest info
  av <- tools::CRAN_package_db()
  av <- av[tolower(av$Package) %in% whitelist(), ]
  ## Get what we already know
  current_vers <- current_pkgvers()
  ## Compare latest with known
  pkgs <- av[c("Package", "Version")]
  pkgs$Package <- tolower(pkgs$Package)
  pkgs <- merge(current_vers, pkgs)
  pkgs <- as.data.frame(lapply(pkgs, as.character), stringsAsFactors = FALSE)
  newer <- apply(pkgs, 1, function(x) compareVersion(x["Version"], x["current_vers"]))
  newer <- newer != 0
  av <- av[newer, ]
  if (nrow(av) < 1){
    message("Nothing to update.")
  } else {
    apply(av, 1, write_pkgbuild)
    topdir <- getwd()
    for (pkg in av$Package){
      message("Updating ", pkg)
      dir <- paste0("PKGBUILDS/r-", tolower(pkg))
      setwd(dir)
      srcinfo <- system2("makepkg", "--printsrcinfo",
                        stdout = TRUE)
      writeLines(srcinfo, ".SRCINFO")
      setwd(topdir)
    }
  }
}

add_new_package <- function(name){
  name <- tolower(name)
  system2("git",
          paste0("submodule add --name PKGBUILDS/r-",
                 name,
                 " ssh://aur@aur.archlinux.org/r-",
                 name, ".git PKGBUILDS/r-", name))
  warning("Don't forget to check SystemRequirements")
}
